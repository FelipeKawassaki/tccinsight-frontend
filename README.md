# iBranch
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.3.

## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Further help
To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## NPM Dependences
## Material Desing:
	> Run: npm install --save @angular/material @angular/cdk
	> Run: npm install --save @angular/animations
	> Run: npm install --save hammerjs
	
## Google Maps 
	> Run: npm install --save @types/googlemaps
